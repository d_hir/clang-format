@echo off
setlocal

cd /d %~dp0\..

:: Determine the root directory of the parent repository
for /f %%I in ('git rev-parse --show-toplevel') do set "RootDir=%%~fI"

:: Set the paths to the source and destination directories
set "SourceDir=%~dp0"
set "DestinationDir=%RootDir%"

:: Copy files from the source to the destination directory
echo Installing to "%DestinationDir%"
copy "%SourceDir%\.clang-format" "%DestinationDir%\

endlocal