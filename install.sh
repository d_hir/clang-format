#!/bin/bash

# Set the path to the source directory
source_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd ..

# Determine the root directory of the repository
root_dir=$(git rev-parse --show-toplevel)
# Set the path to the destination directory
destination_dir="$root_dir"

# Copy files from the source to the destination directory
echo "Installing to \"$destination_dir\""
cp -v "$source_dir/.clang-format" "$destination_dir/"
